package com.example.application_one

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dial_pad.*

class DialPadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dial_pad)
        tv_tel.text = intent.getStringExtra("tel").toString()
        //finish()
    }
}
