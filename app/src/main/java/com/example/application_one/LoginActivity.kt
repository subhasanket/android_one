package com.example.application_one

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login_btn.setOnClickListener {
            val intent1 = Intent(this,DialPadActivity::class.java)
            intent1.putExtra("tel","91-7381443523")
            startActivity(intent1)
        }
    }
}
